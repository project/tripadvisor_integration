# TripAdvisor Integration

TripAdvisor Integration currently provides a field type for storing and displaying data retrieved from the TripAdvisor Content API. If you have any ideas on how this can be extended further please let me know!

For a full description of the module, visit the 
[project page](https://www.drupal.org/project/tripadvisor_integration).

Submit bug reports and feature suggestions, or track changes in the
[Issue queue](https://www.drupal.org/project/issues/tripadvisor_integration).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires Drupal core >= 10.0.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-modules).


## Configuration

1. After installing, go to: _/admin/config/content/tripadvisor-integration_.
2. Add TripAdvisor API key to the admin configuration page and also optionally update cache expiration setting.
3. When creating content, add the TripAdvisor ID in the TripAdvisor field in order to retrieve the data for that ID from the TripAdvisor Content API. 
4. Override the default template provided as with any other theme template.

## Maintainers

Current maintainers:
- Joe Kersey ([@joekers](https://www.drupal.org/u/joekers))


