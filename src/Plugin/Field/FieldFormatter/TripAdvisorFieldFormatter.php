<?php

namespace Drupal\tripadvisor_integration\Plugin\Field\FieldFormatter;

use Drupal\Component\Datetime\Time;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Field formatter for TripAdvisor ID field.
 *
 * @FieldFormatter(
 *   id = "tripadvisor_id_formatter",
 *   label = @Translation("TripAdvisor ID"),
 *   field_types = {"tripadvisor_integration_tripadvisor_id"}
 * )
 */
class TripAdvisorFieldFormatter extends FormatterBase {

  /**
   * The cache backend service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheBackend;

  /**
   * The Date time service.
   *
   * @var \Drupal\Component\Datetime\Time
   */
  protected $dateTime;

  /**
   * The module extension list service.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $moduleExtensionList;

  /**
   * Constructor for TripAdvisorFieldFormatter.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheBackend
   *   The cache backend service.
   * @param \Drupal\Component\Datetime\Time $dateTime
   *   The Date time service.
   * @param \Drupal\Core\Extension\ModuleExtensionList $moduleExtensionList
   *   The module extension list service.
   */
  public function __construct(CacheBackendInterface $cacheBackend,
                              Time $dateTime,
                              ModuleExtensionList $moduleExtensionList) {
    $this->cacheBackend = $cacheBackend;
    $this->dateTime = $dateTime;
    $this->moduleExtensionList = $moduleExtensionList;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('cache.factory'),
      $container->get('datetime.time'),
      $container->get('extension.list.module')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $tripadvisor_id = $item->getString();

      $tripadvisor_data = $this->cacheBackend->get('tripadvisor_integration:' . $tripadvisor_id . ':' . $langcode);

      if (empty($tripadvisor_data) || $tripadvisor_data->expire < $this->dateTime->getRequestTime()) {
        $tripadvisor_data = tripadvisor_integration_fetch_content($tripadvisor_id, $langcode);
      }
      else {
        $tripadvisor_data = $tripadvisor_data->data;
      }

      $elements[$delta] = [
        '#theme' => 'tripadvisor_integration',
        '#data' => $tripadvisor_data,
        '#tripadvisor_logo' => $this->moduleExtensionList->getPath('tripadvisor_integration') . '/images/tripadvisor.png',
      ];
    }
    return $elements;
  }

}
